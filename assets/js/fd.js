$(document).ready(function () {
    $(".lazyload").each(function () {
        var imgPath = $(this).attr("data-srcset");
        $(this).attr("srcset", imgPath);
      });
    $("[data-load]").each(function () {
        $(this).load($(this).data("load"), function () { });
      });
});
//SCROLL TOP
$(document).ready(function () {
    $(".angle-up-btn").hide();
  });
  $("body").on("click", ".scroll-from", function (e) {
    e.preventDefault();
    $("html").animate({ scrollTop: 0 }, "slow");
  });
  //NOUI
document.addEventListener("DOMContentLoaded", function (event) {
    var amountSliderLength = $("#amount").length;
    if (amountSliderLength > 0) {
      var amountSlider = document.getElementById("amount");
      var amountControl = document.getElementById("amount-output");
      noUiSlider.create(amountSlider, {
        start: 200000,
        step: 100,
        connect: "lower",
        connect: [true, false],
        range: {
          min: 50000,
          max: 1000000,
        },
      });
      amountSlider.noUiSlider.on("update", function (values, handle) {
        document.getElementById("amount-output").value = Math.round(values);
      });
      amountControl.addEventListener("change", function () {
        amountSlider.noUiSlider.set([this.value, null]);
      });
    }
  
    var tenureSliderLength = $("#tenure").length;
    if (tenureSliderLength > 0) {
      var tenureSlider = document.getElementById("tenure");
      var tenureControl = document.getElementById("tenure-output");
  
      noUiSlider.create(tenureSlider, {
        start: 3.5,
        step: 0.5,
        connect: "lower",
        connect: [true, false],
        range: {
          min: 1,
          max: 6,
        },
      });
      tenureSlider.noUiSlider.on("update", function (values, handle) {
        document.getElementById("tenure-output").value =
          Math.round(values * 10) / 10;
      });
      tenureControl.addEventListener("change", function () {
        tenureSlider.noUiSlider.set([this.value, null]);
      });
    }
  
    var interestSliderLength = $("#interest").length;
    if (interestSliderLength > 0) {
      var interestSlider = document.getElementById("interest");
      var interestControl = document.getElementById("interest-output");
  
      noUiSlider.create(interestSlider, {
        start: 8,
        step: 1,
        connect: "lower",
        connect: [true, false],
        range: {
          min: 0,
          max: 50,
        },
      });
      interestSlider.noUiSlider.on("update", function (values, handle) {
        document.getElementById("interest-output").value =
          Math.round(values * 10) / 10;
      });
      interestControl.addEventListener("change", function () {
        interestSlider.noUiSlider.set([this.value, null]);
      });
    }
  });
//Mobile menu
if ($(window).width() <= 767) {
    $("body").on("click", ".menu-toogle", function () {
      $("body").toggleClass("show-nav");
    });
    $("body").on("click", ".overlay", function () {
      $("body").toggleClass("show-nav");
    });
    $("body").on("click", ".close", function () {
      $("body").toggleClass("show-nav");
    });
  }
//nav
  if ($(window).width() <= 767) {
    if ($(".owl-mob, .owl-tab").length) {
      $(".owl-mob, .owl-tab").owlCarousel({
        loop: false,
        margin: 0,
        responsiveClass: true,
        autoplay: false,
        nav: true,
        dots: false,
        autoHeight: true,
        mouseDrag: true,
        touchDrag: false,
        responsive: {
          0: {
            items: 2,
            stagePadding: 10,
            nav: true,
            dots: false,
            mouseDrag: false,
            touchDrag: true,
            autoWidth: true,
            navText: [
              "<i class='icon-Leftarrow'></i>",
              "<i class='icon-Rightarrow'></i>",
            ],
          },
  
          768: {
            items: 1,
            margin: 10,
            stagePadding: 25,
            nav: true,
            dots: false,
            mouseDrag: false,
            touchDrag: true,
          },
          1000: {
            items: 1,
          },
        },
      });
    }
  }

  $(document).ready(function() {
    // PIE CHART
    if ($('#chart-area').length) {
        var pieData = [{
                value: 300,
                color: "#fdca00",
                highlight: "",
                label: '',
            },
            {
                value: 50,
                color: "#e0e0e0",
                highlight: "",
                label: ''
            },
        ];
        options = {
            segmentShowStroke: true,
            segmentStrokeColor: "rgba(0,0,0,0)",
            segmentStrokeWidth: 1,
            percentageInnerCutout: 80,
            animationSteps: 30,
            animationEasing: "none",
            animateRotate: true,
            animateScale: true,
            tooltips: {
                enabled: false
            },
            legend: {
                display: false
            },
        }
        window.onload = function() {
            var ctx = document.getElementById("chart-area").getContext("2d");
            window.myPie = new Chart(ctx).Pie(pieData, options);
        };
    }


});

if ($(window).width() > 768) {
    $(".faq-head").click(function () {
      if (!$(this).parent(".faq-list").hasClass("active")) {
        $(".faq-list").removeClass("active");
        $(this).parent().addClass("active");

        $(".faq-parah").slideUp();

        $(this).next().slideDown();
      } else {
        $(this).next().slideUp();
        $(".faq-list").removeClass("active");
      }
    });
  } else {
    $(".faq-head").click(function () {
      if (!$(this).parent(".faq-list").hasClass("active")) {
        $(this).parent().toggleClass("active");
        $(this).next().slideDown();
      } else {
        $(this).next().slideToggle();
        $(this).parent().removeClass("active");
      }
    });
  }

  // text box js
$(".txt-box").focus(function () {
    $(this).parent().addClass("focused");
  });
  $(".txt-box").focusout(function () {
    if ($(this).val() === "") {
      $(this).parent().removeClass("focused");
    }
  });
  
  $(".txt-box").each(function () {
    if ($(this).val() != "") {
      $(this).parent().addClass("focused");
    }
  });
  
  $(".floating-select").each(function () {
    $(this).focus(function () {
      $(this).parent().addClass("focused");
    });
    $(this).focusout(function () {
      $(this).parent().removeClass("focused");
      if($(this).val() !== ""){
        $(this).parent().addClass("focused");
      }
    });
  });

  //date picker
if ($(".date-picker").length) {
    $(".date-picker").datepicker({
      changeMonth: true,
      changeYear: true,
    });
  }

  if ($(".date-picker").length) {
    $(".date-picker").change(function () {
      var the_date = $(this).val();
      if (the_date != "") {
        $(this).parent().addClass("focused");
      }
    });
  }

  // Bod Secf
  $(".bod-box").click(function () {
    var mem = $(this).attr("data-member");

    $(document).ready(function () {
      $(".bod-cnt-list").removeClass("active");
      $("bod-cnt_" + mem).addClass("active");
      $(".bod-list").hide();
      $("#bod-list_" + mem).show();
    });
  });

  $(".bod-cnt-list").click(function () {
    var id = $(this).attr("id").split("_")[1];
    $(this).parents(".container").find(".bod-cnt-list").removeClass("active");
    $(this).addClass("active");
    $(this).parents(".container").find(".bod-list").hide();
    // $('.bod-list').fadeOut().hide();
    $("#bod-list_" + id)
      .fadeIn()
      .show();
  });

  // popups
$("[data-target]").click(function (e) {
    e.preventDefault();
    var currentID = $(this).attr("data-target");
    if (currentID == "close") {
      $(this).parents(".modal").hide();
      $(this).parents(".modal").removeClass("show-modal");
    } else {
      $("#" + currentID).show();
      $("#" + currentID).addClass("show-modal");
    }
  });

//nav
$(".owl-mob a").on("click", function () {
  var el = $(this).offset().left;
  if (el > 200) {
    $(".owl-mob .owl-next").click();
  } else if (el < 50) {
    $(".owl-mob .owl-prev").click();
  }
});
$(".owl-mob1 button").on("click", function () {
  var el = $(this).offset().left;
  if (el > 200) {
    $(".owl-mob1 .owl-next").click();
  } else if (el < 50) {
    $(".owl-mob1 .owl-prev").click();
  }
});
$(".owl-tab .item").on("click", function () {
  var el = $(this).offset().left;
  if (el > 150) {
    $(".owl-tab .owl-next").click();
  } else if (el < 50) {
    // $('.owl-tab .owl-prev').click();
  }
});
$(".owl-bdirec .item").on("click", function () {
  var el = $(this).offset().left;
  if (el > 150) {
    $(".owl-bdirec .owl-next").click();
  }
});

if($(".success-slider").length){
  $('.success-slider').owlCarousel({
      loop: true,
      margin: 0,
      nav: true,
      dots: false,
      navText: ["<i class='icon-Leftarrow'></i>","<i class='icon-Rightarrow'></i>"],
      responsive:{
          0:{
              items: 1
          },
          768:{
              items: 1
          },
          992:{
              items: 1
          }
      }
  });
  }
